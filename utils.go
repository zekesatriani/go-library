package libraries

import (
	"archive/zip"
	"bufio"
	"crypto/tls"
	"errors"
	"fmt"
	"io"
	"net/smtp"
	"reflect"
	"regexp"
	"strconv"
	"strings"
	// "math"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"

	cryptomd5 "crypto/md5"
	"github.com/360EntSecGroup-Skylar/excelize"
	email "github.com/jordan-wright/email"
)

func IsNumeric(s string) bool {
	_, err := strconv.ParseFloat(s, 64)
	return err == nil
}

func IsInteger(s string) bool {
	_, err := strconv.Atoi(s)
	return err == nil
}

func ToFloat64(s string, default_value float64) float64 {
	var lnValue float64 = default_value
	tmp, err := strconv.ParseFloat(s, 64)
	if(err == nil){
		lnValue = tmp
	}
	return lnValue
}

func ToInteger64(s string, default_value int64) int64 {
	var liValue int64 = default_value
	tmp, err := strconv.ParseInt(s, 10, 64)
	if(err == nil){
		liValue = tmp
	}
	return liValue
}

func ToDate(s string) time.Time {
	var ldtValue time.Time
	
	re_dmy := regexp.MustCompile("(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)")
	re_ymd := regexp.MustCompile(`\d{4}-\d{2}-\d{2}`)
	if(re_dmy.MatchString(s)){
		layout := "02/01/2006"
		str := s
		tmp, err := time.Parse(layout, str)
		if(err == nil){
			ldtValue = tmp
		}
	}else if(re_ymd.MatchString(s)){
		layout := "2006-01-02"
		str := s
		tmp, err := time.Parse(layout, str)
		if(err == nil){
			ldtValue = tmp
		}
	}
	
	return ldtValue
}

func MD5(s string) string {
	data := []byte(s)
	return fmt.Sprintf("%x",cryptomd5.Sum(data))
}

func GetFieldString(e interface{}, field string) string {
	r := reflect.ValueOf(e)
	f := reflect.Indirect(r).FieldByName(field)
	return f.String()
}

func GetFieldInteger(e interface{}, field string) int {
	r := reflect.ValueOf(e)
	f := reflect.Indirect(r).FieldByName(field)
	return int(f.Int())
}

func InStringSlice(val string, slice []string) (int, bool) {
	for i, item := range slice {
		if item == val {
			return i, true
		}
	}
	return -1, false
}

func SendMail(param map[string]interface{}) error {
	var result_error error

	sender_host, _ := param["sender_host"]
	sender_port, _ := param["sender_port"]
	sender_name, _ := param["sender_name"]
	sender_email, _ := param["sender_email"]
	sender_password, _ := param["sender_password"]
	sender_tls, _ := param["sender_tls"]
	sender_insecure_skip_verify, _ := param["sender_insecure_skip_verify"]

	arr_to, _ := param["to"]
	arr_cc, _ := param["cc"]
	arr_bcc, _ := param["bcc"]

	ls_subject, _ := param["subject"]
	ls_message_text, _ := param["message"]
	ls_message_html, _ := param["message_html"]

	e := email.NewEmail()

	var ls_sender_host string = ""
	var ls_sender_port string = ""
	var ls_sender_email string = ""
	var ls_sender_password string = ""
	var ls_sender_name string = ""
	var ls_sender_tls string = ""
	var ls_sender_insecure_skip_verify bool = true
	
	if(sender_host != nil && strings.TrimSpace(sender_host.(string)) != ""){
		ls_sender_host = sender_host.(string)
	}else{
		ls_sender_host = ""
	}

	if(sender_port != nil && strings.TrimSpace(sender_port.(string)) != ""){
		ls_sender_port = sender_port.(string)
	}else{
		ls_sender_port = "465"
	}

	if(sender_email != nil && strings.TrimSpace(sender_email.(string)) != ""){
		ls_sender_email = sender_email.(string)
	}else{
		ls_sender_email = ""
	}
	
	if(sender_name != nil && strings.TrimSpace(sender_name.(string)) != ""){
		ls_sender_name = sender_name.(string)
	}else{
		ls_sender_name = ""
	}

	if(sender_password != nil && strings.TrimSpace(sender_password.(string)) != ""){
		ls_sender_password = sender_password.(string)
	}else{
		ls_sender_password = ""
	}

	if(sender_tls != nil && strings.TrimSpace(sender_tls.(string)) != ""){
		ls_sender_tls = sender_tls.(string)
	}else{
		ls_sender_tls = "tls"
	}

	if(sender_insecure_skip_verify != nil && strings.TrimSpace(sender_insecure_skip_verify.(string)) != "" && strings.TrimSpace(sender_insecure_skip_verify.(string)) != "true"){
		ls_sender_insecure_skip_verify = false
	}else{
		ls_sender_insecure_skip_verify = true
	}

	e.From = (ls_sender_name + " <" + ls_sender_email + ">")

	if(arr_to != nil && len(arr_to.([]string)) > 0){
		var arr_tmp []string = []string{}
		for _, val :=  range arr_to.([]string) {
			if(strings.TrimSpace(val) != ""){
				arr_tmp =  append(arr_tmp, val)
			}
		}
		e.To = arr_tmp
	}

	
	if(arr_cc != nil && len(arr_cc.([]string)) > 0){
		var arr_tmp []string = []string{}
		for _, val :=  range arr_cc.([]string) {
			if(strings.TrimSpace(val) != ""){
				arr_tmp =  append(arr_tmp, val)
			}
		}
		e.Cc = arr_tmp
	}

	if(arr_bcc != nil && len(arr_bcc.([]string)) > 0){
		var arr_tmp []string = []string{}
		for _, val :=  range arr_bcc.([]string) {
			if(strings.TrimSpace(val) != ""){
				arr_tmp =  append(arr_tmp, val)
			}
		}
		e.Bcc = arr_tmp
	}

	e.Subject = ls_subject.(string)
	
	if(ls_message_text != nil && strings.TrimSpace( ls_message_text.(string)) != ""){
		e.Text = []byte(ls_message_text.(string))
	}

	if(ls_message_html != nil && strings.TrimSpace( ls_message_html.(string)) != ""){
		e.HTML = []byte(ls_message_html.(string))
	}
	
	
	// TLS config
	tlsconfig := &tls.Config {
		InsecureSkipVerify: ls_sender_insecure_skip_verify,
		ServerName: ls_sender_host,
	}
	_=tlsconfig

	if(ls_sender_tls == "tls") {
		result_error = e.SendWithTLS(ls_sender_host+":"+ls_sender_port, smtp.PlainAuth("", ls_sender_email, Decrypt(ls_sender_password), ls_sender_host), tlsconfig)
	}else if(ls_sender_tls == "starttls") {
		result_error = e.SendWithStartTLS(ls_sender_host+":"+ls_sender_port, smtp.PlainAuth("", ls_sender_email, Decrypt(ls_sender_password), ls_sender_host), tlsconfig)
	}else {
		result_error = e.Send(ls_sender_host + ":" + ls_sender_port, smtp.PlainAuth("", ls_sender_email, Decrypt(ls_sender_password), ls_sender_host))
	}
	// fmt.Println("result_error", result_error)

	return result_error

}


func Unzip(src, dest string) error {
	r, err := zip.OpenReader(src)
	if err != nil {
		return err
	}
	defer r.Close()

	for _, f := range r.File {
		rc, err := f.Open()
		if err != nil {
			return err
		}
		defer rc.Close()

		fpath := filepath.Join(dest, f.Name)
		if f.FileInfo().IsDir() {
			os.MkdirAll(fpath, f.Mode())
		} else {
			var fdir string
			if lastIndex := strings.LastIndex(fpath,string(os.PathSeparator)); lastIndex > -1 {
				fdir = fpath[:lastIndex]
			}

			err = os.MkdirAll(fdir, f.Mode())
			if err != nil {
				return err
			}
			f, err := os.OpenFile(
				fpath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
			if err != nil {
				return err
			}
			defer f.Close()
			
			_, err = io.Copy(f, rc)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func RemoveDirectory(dir string) error {
	d, err := os.Open(dir)
	if err != nil {
		return err
	}
	defer d.Close()
	names, err := d.Readdirnames(-1)
	if err != nil {
		return err
	}
	for _, name := range names {
		err = os.RemoveAll(filepath.Join(dir, name))
		if err != nil {
			return err
		}
	}
	err = os.Remove(dir)
	if err != nil {
		return err
	}
	return nil
}

func CopyFile(src, dst string) (int64, error) {
	sourceFileStat, err := os.Stat(src)
	if err != nil {
			return 0, err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return 0, fmt.Errorf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer source.Close()

	destination, err := os.Create(dst)
	if err != nil {
		return 0, err
	}
	defer destination.Close()
	nBytes, err := io.Copy(destination, source)
	return nBytes, err
}

type Column struct {
	Field	string
	Title	string
	Type	string
	MaxLength	int64
	Precision	int64
	IsRequired	bool
	IsReadonly	bool
	Width	float64
	OrderNo	int
}


func ExcelToBulkFile(temp_folder_path string, file_name string, config_columns []map[string]string, prefix_header string, prefix_column string, f_process_each_row func(index_no int64, sheetname string, i int64, count_error_row int64)(bool) ) (map[string]interface{}, error) {
	var final_result map[string]interface{} = make(map[string]interface{})
	var final_err error

	if _, err := os.Stat(temp_folder_path + file_name); os.IsNotExist(err) {
		final_err = errors.New("file not found")
	}

	if(final_err == nil){

		var filename_bulk_insert string = file_name + "-insert-bulk.csv"
		_ = os.Remove(temp_folder_path + filename_bulk_insert)

		var filename_error_data string = file_name + "-error-import-data.csv"
		_ = os.Remove(temp_folder_path + filename_error_data)

		var filename_error_info string = file_name + "-error-import-info.csv"
		_ = os.Remove(temp_folder_path + filename_error_info)


		final_result["filename_bulk_insert"] = filename_bulk_insert

		file_bulk_insert, _ := os.OpenFile(temp_folder_path + filename_bulk_insert, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		datawriter_bulk_insert := bufio.NewWriter(file_bulk_insert)

		file_error_data, _ := os.OpenFile(temp_folder_path + filename_error_data, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		datawriter_error_data := bufio.NewWriter(file_error_data)

		file_error_info, _ := os.OpenFile(temp_folder_path + filename_error_info, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		datawriter_error_info := bufio.NewWriter(file_error_info)

		fmt.Println("Create Bulk : " + temp_folder_path + filename_bulk_insert)

		f, err := excelize.OpenFile(temp_folder_path + file_name)
		if err != nil {
			final_err = err
		}else{

			var lb_continue = true
			var lb_valid_header bool = false
			var rs_columns []Column = []Column{}
			var index_no int64 = 0
			var count_blank_row int64 = 0
			var count_error_row int64 = 0

			for itter_sheet, sheet_name := range f.GetSheetMap() {
				rows, err := f.Rows(sheet_name)
				if err != nil {
					final_err = err
				}else if(lb_continue) {
					rs_columns = []Column{}
					var i int64 = 0
					count_blank_row = 0

					for rows.Next() {
						if rows.Error() != nil {
							break
						}else if(count_blank_row >= 2){
							break
						}else if(final_err != nil){
							break
						}else if(!lb_continue){
							break
						}

						row := rows.Columns()
						if err != nil {
							break
						}else{
							if(i == 0){
								if(itter_sheet > 0){
									var ls_delimited_line_bulk_insert string = prefix_header
									if(ls_delimited_line_bulk_insert != ""){
										ls_delimited_line_bulk_insert += "|"
									}
									ls_delimited_line_bulk_insert += "index_no"

									var ls_line = ""
									for j, colCell := range row {
										
										var keyword_field string = strings.ToLower(strings.TrimSpace(colCell))

										if(j > 0){
											ls_line += "|"
										}
										ls_line += keyword_field

										for _, rowCell := range config_columns {
											if(rowCell["st_upload"] == "1" && (keyword_field == rowCell["field_name"] || keyword_field == strings.ToLower(rowCell["field_label"]) || strings.Contains(rowCell["field_keyword"], "|" + keyword_field + "|"))){
												rs_columns =  append(rs_columns, Column{
													Field: rowCell["field_name"],
													Title: rowCell["field_label"],
													Type: rowCell["type_data"],
													MaxLength: ToInteger64(rowCell["character_length"],0),
													Precision: ToInteger64(rowCell["field_name"],0),
													IsRequired: (rowCell["st_required"] == "1"),
													IsReadonly: (rowCell["st_readonly"] == "1"),
													OrderNo: j,
												})

												if(ls_delimited_line_bulk_insert != ""){
													ls_delimited_line_bulk_insert += "|"
												}
												ls_delimited_line_bulk_insert += rowCell["field_name"]
												lb_valid_header = true
											}else{
												rs_columns =  append(rs_columns, Column{})
											}
										}
									}

									if(!lb_valid_header){
										final_err = errors.New("Invalid Column Header")
									}else{

										final_result["columns"] = rs_columns

										_, err = datawriter_bulk_insert.WriteString(ls_delimited_line_bulk_insert)
										_, err = datawriter_error_data.WriteString(ls_line)
									}
								}
							} else if(!lb_valid_header || len(rs_columns) == 0 || count_blank_row > 5){
								break
							} else {
								
								if(lb_continue){
									lb_continue = f_process_each_row(index_no, sheet_name, i, count_error_row)
								}

								var ls_line = ""
								var ls_delimited_line_bulk_insert string = prefix_column
								if(ls_delimited_line_bulk_insert != ""){
									ls_delimited_line_bulk_insert += "|"
								}
								ls_delimited_line_bulk_insert += (fmt.Sprintf("%v", index_no))

								var li_max_col = len(rs_columns)
								var lb_rowHasValue = false
								var lb_rowHasError = false

								var validation_status string = "1"
								var validation_message string = "|"

								for j, colCell := range row {
									ls_value := strings.TrimSpace(fmt.Sprintf("%v", colCell))

									if(j > 0){
										ls_line += "|"
									}
									ls_line += ls_value

									if(j < li_max_col && rs_columns[j].Field != ""){
										col := rs_columns[j]

										if(ls_value != ""){
											lb_rowHasValue = true
		
											if(col.Type == "date"){
												ldt_value := ToDate(ls_value)
												if(ToInteger64(ldt_value.Format("2006"),0) <= 1900){
													validation_status = "0"
													validation_message += col.Field + "=Tanggal tidak sesuai|"
		
													lb_rowHasError = true
												}else{
													ls_value = ldt_value.Format("2006-01-02")
												}
											}else if(col.Type == "int"){
												if(!IsInteger(ls_value)){
													validation_status = "0"
													validation_message += col.Field + "=Harus diisi bilangan bulat (integer)|"
													
													lb_rowHasError = true
												}else if(int64(len(ls_value)) > col.MaxLength){
													validation_status = "0"
													validation_message += col.Field + "=Tidak boleh lebih dari " + fmt.Sprintf("%v",col.MaxLength) + " digit|"
													
													lb_rowHasError = true
												}
											}else if(col.Type == "number"){
												if(!IsInteger(ls_value) && !IsNumeric(ls_value)){
													validation_status = "0"
													validation_message += col.Field + "=Harus diisi angka|"
													lb_rowHasError = true
		
												}else if(int64(len(ls_value)) > col.MaxLength){
													validation_status = "0"
													validation_message += col.Field + "=Tidak boleh lebih dari " + fmt.Sprintf("%v",col.MaxLength) + " digit|"
													
													lb_rowHasError = true
												}
											}else if(col.Type == "string"){
												if(int64(len(ls_value)) > col.MaxLength){
													validation_status = "0"
													validation_message += col.Field + "=Jumlah karakter tidak boleh lebih dari " + fmt.Sprintf("%v",col.MaxLength) + "|"
													
													lb_rowHasError = true
												}
											}
		
										}else if(col.IsRequired){
											validation_status = "0"
											validation_message += col.Field + "=Tidak boleh kosong|"
											
											lb_rowHasError = true
										}
		
										ls_delimited_line_bulk_insert += "|" + ls_value
									}
								}
								
								if(lb_rowHasValue){

									if(validation_status == "1"){
										ls_delimited_line_bulk_insert += "|" + validation_status + `|"` + validation_message + `"`
										_, err = datawriter_bulk_insert.WriteString("\r\n" + ls_delimited_line_bulk_insert)	
									}else{
										_, err = datawriter_error_data.WriteString("\r\n" + ls_line)
										_, err = datawriter_error_info.WriteString("\r\nSheet [" + sheet_name + "], Line " + fmt.Sprintf("%v", i) + " :" + validation_message)
									}
		
									if(lb_rowHasError){
										count_error_row++
									}
		
									index_no++
								}else{
									count_blank_row++
								}
							}
						}

						i++
					} // End of Rows Loop
				}
			} // End of Sheet Loop


			if(!lb_continue){

				datawriter_bulk_insert.Flush()
				file_bulk_insert.Close()

				datawriter_error_data.Flush()
				file_error_data.Close()

				datawriter_error_info.Flush()
				file_error_info.Close()

			}else{

				f_process_each_row(index_no, "", -1, count_error_row)
				final_result["index_no"] = fmt.Sprintf("%v", index_no)

				datawriter_bulk_insert.Flush()
				file_bulk_insert.Close()

				datawriter_error_data.Flush()
				file_error_data.Close()

				datawriter_error_info.Flush()
				file_error_info.Close()


				var ls_file_name_archive = file_name + "-error-import.zip"
				var ls_archive_path = temp_folder_path + ls_file_name_archive
				_ = os.Remove(ls_archive_path)

				if(count_error_row == 0){
					_ = os.Remove(temp_folder_path + filename_error_data)
					_ = os.Remove(temp_folder_path + filename_error_info)
				}else{
					final_result["file_name_archive"] = ls_file_name_archive

					// Get a Buffer to Write To
					outFile, err := os.Create(ls_archive_path)
					if err != nil {
						final_err = err
						fmt.Println(err)
					}
					defer outFile.Close()

					// Create a new zip archive.
					w := zip.NewWriter(outFile)


					// Add some files to the archive.
					dat, err := ioutil.ReadFile(temp_folder_path + filename_error_data)
					if err != nil {
						final_err = err
						fmt.Println(err)
					}
					f, err := w.Create(filename_error_data)
					if err != nil {
						final_err = err
						fmt.Println(err)
					}
					_, err = f.Write(dat)
					if err != nil {
						final_err = err
						fmt.Println(err)
					}


					// Add some files to the archive.
					dat, err = ioutil.ReadFile(temp_folder_path + filename_error_info)
					if err != nil {
						final_err = err
						fmt.Println(err)
					}
					f, err = w.Create(filename_error_info)
					if err != nil {
						final_err = err
						fmt.Println(err)
					}
					_, err = f.Write(dat)
					if err != nil {
						final_err = err
						fmt.Println(err)
					}


					// Make sure to check the error on Close.
					err = w.Close()
					if err != nil {
						final_err = err
						fmt.Println(err)
					}else{
						_ = os.Remove(temp_folder_path + filename_error_data)
						_ = os.Remove(temp_folder_path + filename_error_info)
					}
				}

			}
		}
	}

	return final_result, final_err
}